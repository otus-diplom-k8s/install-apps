# Установка инфраструктурных приложений

## 1. Установка Gitlab-agent для подключения кластера

Создаём директорию .gitlab/agents/otus-k8s-diplom/ и добавляем файл config.yaml с таким содержимым

```
ci_access:
  groups:
    - id: otus-diplom-k8s/
```

Добавляем файл в репотизорий.

В репотизории заходим в вкладку Operate/Kubernetes Clusters и выбираем наш агент. Там будет команда установки:

```
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install gitlab-agent gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set image.tag=v16.10.0-rc1 \
    --set config.token=token \
    --set config.kasAddress=wss://kas.gitlab.com
```

Устанавливаем Gitlab-agent. После успешной установки, в вкладке Operate/Kubernetes Clusters Connections Status загорится зелёным. 

## 2. Установка Ingress Nginx

Установка описана в файле .gitlab-ci.yml в stage deploy-ingress-nginx

## 3. Установка Cert manager

Установка описана в файле .gitlab-ci.yml в stage deploy-cert-manager

## 4. Установка prometheus-operator

Установка описана в файле .gitlab-ci.yml в stage prometheus-operator

## 5. Установка loki/promtail

Установка описана в файле .gitlab-ci.yml в stage deploy-loki/promtail


## 6. Установка приложений hipstershop

Установка описана в файле .gitlab-ci.yml в stage deploy hipstersho

В данных  установках добавлено Ingress правило с добавлением сертификатов от Let's Encrypt

Ссылки:

https://alertmanager.158.160.148.46.nip.io/

https://grafana.158.160.148.46.nip.io/

https://prometheus.158.160.148.46.nip.io/

https://shop.158.160.148.46.nip.io/

## 7. Настройка мониторинга, просмотр логов и алертина в Grafana

При установке prometheus-operator были добавлены dashboard для k8s.

![monitoring](monitoring/pictures/1.PNG)

Мониторить будем ресуры нод

![monitoring](monitoring/pictures/2.PNG)

Мониторить будем ns

![monitoring](monitoring/pictures/3.PNG)

Мониторить будем ресуры под

![monitoring](monitoring/pictures/4.PNG)

Мониторить будем kubelet

![monitoring](monitoring/pictures/5.PNG)

Мониторить будем сетевую систему  кластера

![monitoring](monitoring/pictures/6.PNG)

Мониторить будем PV

![monitoring](monitoring/pictures/7.PNG)

Мониторить будем потребление кластера

![monitoring](monitoring/pictures/8.PNG)

Также, настроен nodeexporter для нод  k8s

![monitoring](monitoring/pictures/9.PNG)

Также, для просмотра логов, мы поставили Loki. Логи можно смотреть во вкладке Explore

![monitoring](monitoring/pictures/10.PNG)

Также, при установки prometheus-operator были добавлены Alerts 

![monitoring](monitoring/pictures/11.PNG)

Оповещения настроены в Telegram в интерфейсе Grafana. 

![monitoring](monitoring/pictures/12.PNG)

![monitoring](monitoring/pictures/13.PNG)

Оповещения протестированы

![monitoring](monitoring/pictures/14.PNG)
